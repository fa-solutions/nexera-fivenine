// (function () {

const phoneAppletPrefixName = "five9";

const PHONE_FIELD_TYPES = [
    { field: 'work_phone', type: 'business' },
    { field: 'mobile_phone', type: 'mobile' },
    { field: 'home_phone', type: 'home' },
    { field: 'alternate_phone_1', type: 'other' },
];

const phoneAppletFields = {
    fnId: `${phoneAppletPrefixName}_field0`,
    contact: `${phoneAppletPrefixName}_field1`,
    from: `${phoneAppletPrefixName}_field2`,
    to: `${phoneAppletPrefixName}_field3`,
    duration: `${phoneAppletPrefixName}_field4`,
    note: `${phoneAppletPrefixName}_field5`,
    direction: `${phoneAppletPrefixName}_field6`,
    disposition: `${phoneAppletPrefixName}_field7`,
    handleTime: `${phoneAppletPrefixName}_field8`,
    callEndReason: `${phoneAppletPrefixName}_field9`,
    subject : `${phoneAppletPrefixName}_field10`,
    campaignName : `${phoneAppletPrefixName}_field11`,
    owner: 'owner_id'
}
